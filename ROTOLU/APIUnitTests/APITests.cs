using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using ROTOLU.Controllers;
using ROTOLU.Messages;

namespace APIUnitTests
{
    public class Tests
    {
        // we should mock the logger here, but this is fine for now...
        private readonly ILogger<ReservationsController> _logger = new NullLogger<ReservationsController>();

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void GetStatusTest()
        {
            var reservationsController = new ReservationsController(_logger);
            try
            {
                StatusMessage result = reservationsController.GetStatus();
                Assert.That(result, Is.Not.Null);
            }
            catch
            {
                Assert.Fail();
            }
        }

        // doesn't work currently as we're not providing the database in the unit test
        // environment and don't mock the database access to, so temporarily disable this test
        //
        //[Test]
        //public void GetAll()
        //{
        //    ReservationsController reservationsController = new ReservationsController(_logger);
        //    try
        //    {
        //        List<Reservation> reservations = reservationsController.Get();
        //        if (reservations.Count > 0)
        //        {
        //            Assert.Pass();
        //        }
        //        else
        //        {
        //            Assert.Fail();
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}
    }
}