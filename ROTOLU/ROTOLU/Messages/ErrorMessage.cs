namespace ROTOLU.Messages
{
    public class ErrorMessage
    {
        public ErrorMessage(string? message = null, object? additionalProp1 = null)
        {
            Message = message;
            AdditionalProp1 = additionalProp1;
        }

        public string? Message { get; }

        public object? AdditionalProp1 { get; }
    }
}