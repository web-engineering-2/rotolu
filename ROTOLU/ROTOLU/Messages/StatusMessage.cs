﻿namespace ROTOLU.Messages
{
    public class StatusMessage
    {
        public StatusMessage(List<string>? authors, string? apiVersion, object? additionalProb1)
        {
            Authors = authors;
            ApiVersion = apiVersion;
            AdditionalProb1 = additionalProb1;
        }

        public List<string>? Authors { get; }

        public string? ApiVersion { get; }

        public object? AdditionalProb1 { get; }
    }
}
