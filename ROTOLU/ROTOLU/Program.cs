using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Logging;
using ROTOLU;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();


builder.Services.AddControllers(options =>
{
    options.Filters.Add<HttpResponseExceptionFilter>();
});

string realm = Environment.GetEnvironmentVariable("KEYCLOAK_REALM") ?? "biletado";
string host = Environment.GetEnvironmentVariable("KEYCLOAK_HOST") ?? "localhost";
string issuer = "localhost";
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, o =>
            {
                o.MetadataAddress = $"http://{host}/auth/realms/{realm}/.well-known/openid-configuration";
                o.Authority = $"http://{host}/realms/{realm}";
                o.TokenValidationParameters.ValidIssuer = $"http://{issuer}/auth/realms/{realm}";
                o.TokenValidationParameters.ClockSkew = TimeSpan.Zero; // setting this to zero reduces the time tokens are seen valid after their expiration
                o.Audience = "account";
                o.RequireHttpsMetadata = false;
            });

IdentityModelEventSource.ShowPII = true;

var app = builder.Build();

app.UseAuthentication();

if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
    IdentityModelEventSource.ShowPII = true;
    app.UseExceptionHandler("/error-development");
}
else
{
    app.UseExceptionHandler("/error");
}

// app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
