using System.Reflection;
using Microsoft.AspNetCore.Mvc;
using ROTOLU.DB;
using ROTOLU.DB.Entities;
using Microsoft.AspNetCore.Authorization;
using ROTOLU.Messages;

namespace ROTOLU.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ReservationsController : ControllerBase
    {
        private readonly ILogger<ReservationsController> _logger;

        public ReservationsController(ILogger<ReservationsController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public List<Reservation> Get(Guid? roomId, DateTime? before, DateTime? after)
        {
            try
            {
                before = before?.ToUniversalTime();
                after = after?.ToUniversalTime();
                List<Reservation> reservations = ReservationDataStore.GetReservations(roomId, before, after).ToList<Reservation>();
                return reservations;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                throw;
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetWithID(Guid id)
        {
            try
            {
                Reservation? reservation = ReservationDataStore.GetReservation(id);
                if (reservation is null)
                {
                    return new NotFoundObjectResult(new ErrorMessage("not found", new object()));
                }

                return new OkObjectResult(reservation);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                throw;
            }
        }

        [HttpGet("status")]
        public StatusMessage GetStatus()
        {
            var authors = new List<string>() {
                "TW",
                "RA",
                "LW"
            };
            string? apiVersion = Assembly.GetExecutingAssembly().GetName().Version?.ToString();
            var statusReturn = new StatusMessage(authors, apiVersion, new object());

            return statusReturn;
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Post(Reservation reservation)
        {
            try
            {
                //authentication stuff
                if (reservation.Id.ToString() == "")
                {
                    reservation.Id = Guid.NewGuid();
                }

                // convert date/time to UTC, to prevent timezones issues
                reservation.From.ToUniversalTime();
                reservation.To.ToUniversalTime();

                // check if room exists
                if (!await HelperClass.AskForRoom(reservation.RoomId))
                {
                    var errorMessage = new ObjectResult(new ErrorMessage("room not found", new object()))
                    {
                        StatusCode = StatusCodes.Status422UnprocessableEntity
                    };
                    return errorMessage;
                }

                // check if room is already reserved
                IEnumerable<Reservation> reservationsInTimeFrame = ReservationDataStore.GetReservations(reservation.RoomId).Where(selectReservation =>
                    Convert.ToDateTime(selectReservation.From) <= Convert.ToDateTime(reservation.To) &&
                    Convert.ToDateTime(selectReservation.From) >= Convert.ToDateTime(reservation.From) ||
                    Convert.ToDateTime(selectReservation.To) <= Convert.ToDateTime(reservation.To) &&
                    Convert.ToDateTime(selectReservation.To) >= Convert.ToDateTime(reservation.From)
                );

                if (reservationsInTimeFrame.Any())
                {
                    var errorMessage = new ObjectResult(new ErrorMessage("conflicts with other reservation on the same room", new object()))
                    {
                        StatusCode = StatusCodes.Status409Conflict
                    };
                    return errorMessage;
                }

                ReservationDataStore.AddOrUpdateReservation(reservation);

                var response = new ObjectResult(reservation)
                {
                    StatusCode = StatusCodes.Status201Created
                };
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                throw;
            }
        }

        [Authorize]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutReservation(Guid id, Reservation reservation)
        {
            try
            {
                // convert date/time to UTC, to prevent timezones issues
                reservation.From.ToUniversalTime();
                reservation.To.ToUniversalTime();

                //authentication stuff

                // check for id match
                if (!id.Equals(reservation.Id))
                {
                    var errorMessage = new ObjectResult(new ErrorMessage("mismatching id", new object()))
                    {
                        StatusCode = StatusCodes.Status422UnprocessableEntity
                    };
                    return errorMessage;
                }

                // check if room exists
                if (await HelperClass.AskForRoom(reservation.RoomId))
                {
                    var errorMessage = new ObjectResult(new ErrorMessage("room not found", new object()))
                    {
                        StatusCode = StatusCodes.Status422UnprocessableEntity
                    };
                    return errorMessage;
                }

                // check if room is already reserved
                IEnumerable<Reservation> reservationsInTimeFrame = ReservationDataStore.GetReservations(reservation.RoomId, reservation.To, reservation.From);

                if (reservationsInTimeFrame.Any())
                {
                    var errorMessage = new ObjectResult(new ErrorMessage("conflicts with other reservation on the same room", new object()))
                    {
                        StatusCode = StatusCodes.Status409Conflict
                    };
                    return errorMessage;
                }

                ReservationDataStore.AddOrUpdateReservation(reservation);

                return new StatusCodeResult(204);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                throw;
            }
        }

        [Authorize]
        [HttpDelete("{id}")]
        public IActionResult DeleteReservation(Guid id)
        {
            try
            {
                //authentication stuff

                if (!ReservationDataStore.DeleteReservation(id))
                {
                    return new NotFoundObjectResult(new ErrorMessage("reservation not found", new object()));
                }

                return new StatusCodeResult(204);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                throw;
            }
        }
    }
}