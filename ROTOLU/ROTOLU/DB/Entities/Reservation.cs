﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ROTOLU.DB.Entities
{
    [Table("reservations")]
    public class Reservation
    {
        [Key]
        [Column("id")]
        public Guid Id { get; set; }

        [Required]
        [Column("from")]
        public DateTime From { get; set; } = DateTime.UtcNow;

        [Required]
        [Column("to")]
        public DateTime To { get; set; } = DateTime.UtcNow.AddDays(1);

        [Required]
        [Column("room_id")]
        public Guid RoomId { get; set; }
    }
}
