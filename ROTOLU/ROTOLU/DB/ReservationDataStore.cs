﻿using Microsoft.EntityFrameworkCore;
using ROTOLU.DB.Entities;

namespace ROTOLU.DB
{
    public static class ReservationDataStore
    {
        /// <summary>
        /// get all reservations
        /// </summary>
        /// <param name="roomId">filter for specified room id</param>
        /// <param name="before">filter for reservations before specified day</param>
        /// <param name="after">filter for reservations after specified day</param>
        /// <returns>all reservations matching the filter criteria</returns>
        public static IEnumerable<Reservation> GetReservations(Guid? roomId = null, DateTime? before = null, DateTime? after = null)
        {
            using var reservationsContext = new ReservationsContext();
            return reservationsContext.Reservations
                .Where(reservation => roomId == null || roomId.Equals(reservation.RoomId))
                .Where(reservation => before == null || reservation.From <= before)
                .Where(reservation => after == null || reservation.To >= after)
                .ToList();
        }

        /// <summary>
        /// find reservation by id
        /// </summary>
        /// <param name="id">the id of the reservation to get</param>
        /// <returns>the reservation, or null if not found</returns>
        public static Reservation? GetReservation(Guid id)
        {
            using var reservationsContext = new ReservationsContext();
            return reservationsContext.Reservations.Find(id);
        }

        /// <summary>
        /// add or update a reservation
        /// </summary>
        /// <param name="reservation">the reservation to add or update</param>
        public static void AddOrUpdateReservation(Reservation reservation)
        {
            using var reservationsContext = new ReservationsContext();
            Reservation? res = reservationsContext.Reservations.Find(reservation.Id);
            if (res != null) // update
            {
                res.RoomId = reservation.RoomId;
                res.From = reservation.From;
                res.To = reservation.To;
            }
            else
            {
                reservationsContext.Reservations.Add(reservation);
            }
            reservationsContext.SaveChanges();
        }

        /// <summary>
        /// deletes a reservation by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>true if reservation was deleted, false if reservation was not found</returns>
        public static bool DeleteReservation(Guid id)
        {
            using var reservationsContext = new ReservationsContext();
            int affected = reservationsContext.Reservations.Where(reservation => id.Equals(reservation.Id)).ExecuteDelete();
            return affected == 1;
        }
    }
}
