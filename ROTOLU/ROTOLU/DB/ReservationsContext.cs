﻿using Microsoft.EntityFrameworkCore;
using ROTOLU.DB.Entities;

namespace ROTOLU.DB
{
    public class ReservationsContext : DbContext
    {
        private static readonly string _connectionString = GetConnectionString();

        public DbSet<Reservation> Reservations { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(_connectionString);
        }

        private static string GetConnectionString()
        {
            string Host = Environment.GetEnvironmentVariable("POSTGRES_RESERVATIONS_HOST") ?? "localhost";
            string Port = Environment.GetEnvironmentVariable("POSTGRES_RESERVATIONS_PORT") ?? "5432";
            string DbName = Environment.GetEnvironmentVariable("POSTGRES_RESERVTIONS_DBNAME") ?? "reservations";
            string Username = Environment.GetEnvironmentVariable("POSTGRES_RESERVATIONS_USER") ?? "postgres";
            string Password = Environment.GetEnvironmentVariable("POSTGRES_RESERVATIONS_PASSWORD") ?? "postgres";
            return $"Host={Host};Port={Port};Database={DbName};Username={Username};Password={Password}";
        }
    }
}
