﻿namespace ROTOLU
{
    public class HelperClass
    {
        public static async Task<bool> AskForRoom(Guid room_id)
        {
            string AssetsUri = Environment.GetEnvironmentVariable("ASSETS_ROOM_URI") ?? "http://localhost/api/assets/rooms/";

            var httpClient = new HttpClient
            {
                BaseAddress = new Uri(AssetsUri)
            };
            using HttpResponseMessage response = await httpClient.GetAsync(room_id.ToString());
            try
            {
                response.EnsureSuccessStatusCode();
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }
    }
}