# Dokumentation - Web Engineering II
# Robin Augenstein, Tom Witzel, Lukas Wunderlich
#

Diese Dokumentation dient als technische Beschreibung der Dateien des Gitlab Repositories [ROTOLU](https://gitlab.com/web-engineering-2/rotolu/-/tree/main "Hier klicken um zum Repository zu kommen")  

Wir haben uns als Gruppe dafür entschieden, eine [MIT license](https://gitlab.com/web-engineering-2/rotolu/-/blob/main/LICENSE) zu nutzen. 
Somit sind sämtliche Verwendungszwecke, inklusiver der kommerziellen Nutzung, z.B. im Rahmen der Web-Engineering-II Vorlesung erlaubt. 
Die Entwickler übernehmen keine Haftung für mögliche Fehler in der Software, bieten keine Gewährleistung und bieten keine verpflichtenden Serviceleistungen an.  
Die Nutzung der Software ist nur gestattet, sofern die von uns erteilte Lizenz weiterverwendet wird. Eine Änderung der Lizenz für unseren Code ist unzulässig.

Um die gewünschte Automatisierung des Build Prozesses zu gewährleisten (continuous integration / CI), gibt es die Datei [.gitlab-ci.yml](https://gitlab.com/web-engineering-2/rotolu/-/blob/main/.gitlab-ci.yml).  
Diese Datei besteht aus mehreren Teilen. Der erste Teil ist der Build Prozess selbst.  
Hier wird das offizielle dotnet image von Microsoft verwendet.  
Der zweite Teil befasst sich mit dem Thema testing.  
Dafür muss zunächst die Build Phase abgeschlossen sein. Anschließend wird ein Unit Test gestartet. Wenn dieser Test beendet wurde, wird das Ergebnis im Log gespeichert.  
Nach einem erfolgreichen Testdurchlauf wird der Docker Container gestartet. Dafür werden die Variablen `CI_REGISTRY_USER` `CI_REGISTRY_PASSWORD` und `CI_REGISTRY` benötigt. Abschließend wird überprüft, welcher Branch derzeit genutzt wird und der Dockercontainer entsprechend gestartet. Jedoch nur, wenn der branch eine Dockerfile enthält.

In der Datei [.gitignore](https://gitlab.com/web-engineering-2/rotolu/-/blob/main/.gitignore) werden sämtliche Dateien definiert, die bei einem commit nicht in das Repository gepusht werden sollen. 
Dazu gehören unter anderem überflüssige Dateien der Entwicklungsumgebung und nutzerspezifische Dateien.

Der gesamte Ordner [.vscode](https://gitlab.com/web-engineering-2/rotolu/-/tree/main/.vscode) enthält VSCode spezifische Dateien.

Die Datei [ROTOLU.sln](https://gitlab.com/web-engineering-2/rotolu/-/blob/main/ROTOLU/ROTOLU.sln) enthält weitere Informationen zur Version von Visual Studio und dem Projekt sowie zur pre und post Solution.

In unserer [Dockerfile](https://gitlab.com/web-engineering-2/rotolu/-/blob/main/ROTOLU/Dockerfile) wird unter anderem nach dem Wechsel in das /app Verzeichnis, der zu verwendende Port beschrieben. 
Als Base-Image wird das Microsoft dotnet aspnet 6.0 Image verwendet.  
Anschließend verwenden wir das dotnet SDK in der Version 6.0 (ebenfalls von Microsoft) als Base Image, wechseln in das Verzeichnis /src und kopieren die ROTOLU.csproj Datei in den Ordner ROTOLU.  
Danach stellen wir die Datei wieder her, kopieren erneut und wechseln in das Verzeichnis /src/ROTOLU.  
Dort lassen wir den Befehl *dotnet build "ROTOLU.csproj"* laufen und publishen das ganze anschließend.

In der Datei [.dockerignore](https://gitlab.com/web-engineering-2/rotolu/-/blob/main/ROTOLU/.dockerignore) werden die Dateien aufgeführt, die Docker bei der Erstellung des Images nicht berücksichtigen soll.

[appsettings.json](https://gitlab.com/web-engineering-2/rotolu/-/blob/main/ROTOLU/ROTOLU/appsettings.json) beinhaltet Einstellungen zum Logging und der Auffindbarkeit der Seite. 
Die Seite soll unter jeder Adresse (auf der ein solcher Server läuft), aber nur unter dem Port **9000** zu finden sein.

[appsettings.Development.json](https://gitlab.com/web-engineering-2/rotolu/-/blob/main/ROTOLU/ROTOLU/appsettings.Development.json) hat nahezu denselben Inhalt, verzichtet aber auf die Zeile **"AllowedHosts": "\*",**

[ROTOLU.csproj](https://gitlab.com/web-engineering-2/rotolu/-/blob/main/ROTOLU/ROTOLU/ROTOLU.csproj) beinhaltet die Referenzen zu den benötigten Packages sowie einige weitere Einstellungen.

[Program.cs](https://gitlab.com/web-engineering-2/rotolu/-/blob/main/ROTOLU/ROTOLU/Program.cs) erstellt die lauffähige App und beinhaltet einen Aufruf der externen Authentifizierungsmethode.

[HttpResponseExceptionFilter.cs](https://gitlab.com/web-engineering-2/rotolu/-/blob/main/ROTOLU/ROTOLU/HttpResponseExceptionFilter.cs) filtert die HTTP Fehlermeldungen heraus, damit diese von **HttpResponseExceptions.cs** verarbeitet werden können.

[HttpResponseExceptions.cs](https://gitlab.com/web-engineering-2/rotolu/-/blob/main/ROTOLU/ROTOLU/HttpResponseException.cs) gibt die Fehlermeldung aus.


Die [HelperClass.cs](https://gitlab.com/web-engineering-2/rotolu/-/blob/main/ROTOLU/ROTOLU/HelperClass.cs) Datei wird zum Datenaustausch mit der Assets API verwendet und fragt ab, ob ein Raum auch wirklich existiert. 

[launchSettings.json](https://gitlab.com/web-engineering-2/rotolu/-/blob/main/ROTOLU/ROTOLU/Properties/launchSettings.json) enthält die Profile "_ROTOLU_" "_IIS EXPRESS_" und "_Docker_". 
Dort werden Eigenschaften wie z.B. Umgebungsvariablen oder die launchURL festgelegt. 

[StatusMessage.cs](https://gitlab.com/web-engineering-2/rotolu/-/blob/main/ROTOLU/ROTOLU/Messages/StatusMessage.cs) enthält Variablen für die API Version, Autoren und weitere Informationen. 
Das Ergebnis finden Nutzer unter dem Reiter "API Status". 

[ErrorMessage.cs](https://gitlab.com/web-engineering-2/rotolu/-/blob/main/ROTOLU/ROTOLU/Messages/ErrorMessage.cs) dient der Fehlerbehandlung bzw. Fehleradressierung. 
Sollte es ein Problem mit vorhandenen APIs geben wird die Fehlermeldung als Status angezeigt.

[ReservationsContext](https://gitlab.com/web-engineering-2/rotolu/-/blob/main/ROTOLU/ROTOLU/DB/ReservationsContext.cs) bietet Umgebungsvariablen wie *Host*, *Port*, *Name der Datenbank*, *Nutzername des Nutzers* und das *Passwort*.  
Diese Metadaten sind für die Verbindung zur Datenbank relevant.
Die Informationen können von anderen Methoden aus anderen Dateien wie z.B. der **ReservationDataStorage.cs** oder **ReservationsController.cs** genutzt werden.

[ReservationDataStorages.cs](https://gitlab.com/web-engineering-2/rotolu/-/blob/main/ROTOLU/ROTOLU/DB/ReservationDataStore.cs) enthält mehrere Methoden bzw. Datenbankabfragen.  
Mit der Methode **"GetReservations"** können alle oder nur bestimmte (per ID definierte) Reservierungen gefunden werden.  
Außerdem gibt es noch die Methode **"AddOrUpdateReservation"** mit der Reservierungen verändert oder hinzugefügt werden können. 
Wird eine ID mitgegeben, so wird eine entsprechende Reservierung gesucht und mit den mitgelieferten Informationen (RoomId, From und To) überarbeitet. 
Falls keine ID angegeben wurde, wird ein neuer Eintrag erstellt.
Am Ende der Methode werden die Änderungen gespeichert und das Event erstellt oder aktualisiert.  
Natürlich darf auch eine Methode zum Löschen abgesagter Reservierungen nicht fehlen. 
Wird eine Reservierung mit der angegebenen ID gefunden, wird sie gelöscht und ein `true` zurückgegeben. 
Ist keine Reservierung mit dieser ID vorhanden, so ist der Returnwert `false`.  
Dank Optimierung wird der größtmögliche Teil direkt auf der Datenbank ausgeführt.

[Reservations.cs](https://gitlab.com/web-engineering-2/rotolu/-/blob/main/ROTOLU/ROTOLU/DB/Entities/Reservation.cs) ist ein Entity für Reservierungen. Die Daten des Entities sind:

| Entity | Beschreibung |
| --- | --- |
| `id` | Reservierungs ID |
| `from` | Beginn der Reservierung |
| `to` | Ende der Reservierung |
| `room_id` | ID des Raums |

Die [ReservationsController.cs](https://gitlab.com/web-engineering-2/rotolu/-/blob/main/ROTOLU/ROTOLU/Controllers/ReservationsController.cs) Datei beinhaltet einen Logger, eine Methode um Reservierungen auszugeben, Fehlermeldungen sowie das generelle Handling von HTTP Anfragen. 
Werden neue Reservierungen erstellt bzw. bestehende Reservierungen geändert, wird überprüft, ob zum selben Zeitpunkt bereits eine andere Reservierung für denselben Raum vorliegt und notfalls eine Fehlermeldung geworfen. 

[Usings.cs](https://gitlab.com/web-engineering-2/rotolu/-/blob/main/ROTOLU/APIUnitTests/Usings.cs) dient dazu, das Unit Test Framework NUnit einzubinden.

[APIUnitTests.csproj](https://gitlab.com/web-engineering-2/rotolu/-/blob/main/ROTOLU/APIUnitTests/APIUnitTests.csproj) beinhaltet Einstellungen und Informationen rund um den Unit Test.

[APITests.cs](https://gitlab.com/web-engineering-2/rotolu/-/blob/main/ROTOLU/APIUnitTests/APITests.cs) ist der Test, der automatisch laufen soll. 
Durch den Test wird der Status des ReservationController abgefragt, ist der Status nicht Null, so wird der Test als erfolgreich beendet angesehen.

| Name der Umgebungsvariable | Beschreibung |
| --- |--- |
| KEYCLOAK_REALM | Bereich für die Authentifizierung |
| KEYCLOAK_HOST | Authentifizierungs-Host |
| ASSETS_ROOM_URI | Link zum Assets backend |
| POSTGRES_RESERVATIONS_HOST | Datenbank Host |
| POSTGRES_RESERVATIONS_PORT | Datenbank Port |
| POSTGRES_RESERVATIONS_DBNAME | Datenbank Name |
| POSTGRES_RESERVATIONS_USER | Nutzername für den Datenbanknutzer |
| POSTGRES_RESERVATIONS_PASSWORD | Passwort für die Datenbank |